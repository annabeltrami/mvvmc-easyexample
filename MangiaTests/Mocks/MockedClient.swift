//
//  MockedClient.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 25/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

@testable import Mangia

class MockedClient: Client {
   
    var postcode: Postcode?
    var restaurants: [Restaurant]?
    var error: Client.APIError?
     
    var successful = true
    
    override func fetchPostcode(postcode: String, result: @escaping (Result<PostcodeResponse, Client.APIError>) -> Void) {
        if successful {
            self.postcode = Postcode(postcode: postcode, outcode: "SW1A", ced: "Downing Street")
        } else {
            error = .invalidData
        }
    }
    
    override func fetchRestaurants(outcode: String, result: @escaping (Result<RestaurantResponse, Client.APIError>) -> Void) {
        if successful {
            restaurants = [Restaurant(id: 1, name: "Nandos", isOpen: true, ratingStars: nil, cousines: nil, logo: nil)]
        } else {
            error = .invalidData
        }
    }
}
