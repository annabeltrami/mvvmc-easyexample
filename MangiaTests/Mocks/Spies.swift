//
//  MockLocationViewModelDelegate.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 03/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

@testable import Mangia

// MARK: - LocationSelectionViewDelegateSpy

class LocationSelectionViewDelegateSpy: LocationSelectionViewDelegate {
    
    var addressString: String?
    var errorMessage: String?
    
    func didFetchPostcode(for address: String) {
        addressString = address
    }
    
    func failedToFetchPostcode(errorMessage: String?) {
        self.errorMessage = errorMessage
    }
}

// MARK: - LocationSelectionViewModelCoordinatorDelegateSpy

class LocationSelectionViewModelCoordinatorDelegateSpy: LocationSelectionViewModelCoordinatorDelegate {
    
    var outcode: String?
    
    func didGetOutcodeForRestaurants(_ outcode: String) {
        self.outcode = outcode
    }
}

// MARK: - RestaurantListViewDelegateSpy

class RestaurantListViewDelegateSpy: RestaurantListViewDelegate {
    
    var hasFinishedLoadingRestaurants = false
    var errorMessage: String?
    
    func finishedLoadingRestaurants() {
        hasFinishedLoadingRestaurants = true
    }
    
    func failedToLoadRestaurants(errorMessage: String?) {
        self.errorMessage = errorMessage
    }

}
