//
//  MockedLocationManager.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 03/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//
import CoreLocation
@testable import Mangia

// MARK: - MockedLocationManager

class MockedLocationManager: LocationManager {
    
    var postcodeString: String?
    
    override func fetchLocation() {
        postcodeString = "SW1A 2AA"
    }
}

// MARK: - LocationManagerDelegateSpy

class LocationManagerDelegateSpy: LocationManagerDelegate {
   
    var postcodeString: String?
    var error: LocationManagerError?
    
    func didFetchPostcodeFromUserLocation(postcodeString: String) {
        self.postcodeString = postcodeString
    }
    
    func didReceiveErrorFetchingUserPostcodeFromLocation(error: LocationManagerError) {
        self.error = error
    }
}

// MARK: - MockedCLLocationManager

class MockedCLLocationManager: CLLocationManager {
    
    var mockedLocation: CLLocation? = CLLocation(latitude: 51.5033, longitude: -0.1275)
    
    override var location: CLLocation? {
        return mockedLocation
    }

}
