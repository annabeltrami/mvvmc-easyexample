//
//  MockedViewModel.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 03/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

@testable import Mangia

class LocationSelectionViewModelSpy: LocationSelectionViewModel {
   
    var hasSubmittedOutcode = false
    var hasFetchedAndValidatedOutCode = false
    
    override func submitOutcode() {
        hasSubmittedOutcode = true
    }
    
    override func fetchAndValidateOutcode(postcode: String?) {
        hasFetchedAndValidatedOutCode = true
    }
    
}

