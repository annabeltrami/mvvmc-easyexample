//
//  LocationSelectionCoordinatorTests.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import XCTest
@testable import Mangia

class LocationSelectionCoordinatorTests: XCTestCase {
    
    // MARK: Properties
    private var locationSelectionCoordinator: LocationSelectionCoordinator?
    private var mockedClient = MockedClient()
    
    override func setUp() {
        locationSelectionCoordinator = LocationSelectionCoordinator(client: mockedClient, navigationController: UINavigationController())
    }

    override func tearDown() {
        locationSelectionCoordinator = nil
    }

    func test_RootViewControllerNotNil_OnBaseCoordinatorInit() {
        XCTAssertNotNil(locationSelectionCoordinator?.navigationController)
    }
    
    func test_LocationSelectionCoordinatorStartedSuccessfully_WhenBaseCoordinatorIsStarted() throws {
        
        given("The coordinator's navigation controller is the rootCoordinator") {
            XCTAssertEqual(locationSelectionCoordinator?.navigationController.viewControllers.count, 0)
        }
        
        when("The coordinator is started") {
            locationSelectionCoordinator?.start()
        }
        
        try then("The navigation controller of the coordinaror should have a viewcontroller of LocationSelectionViewController type") {
            XCTAssertEqual(locationSelectionCoordinator?.navigationController.viewControllers.count, 1)
            
            let topViewController = try XCTUnwrap(locationSelectionCoordinator?.navigationController.viewControllers[0])
            
            XCTAssert(topViewController is LocationSelectionViewController)
        }

    }
    
    func test_RestaurantsCoordinatorStartedSuccessfully() {
        XCTAssertEqual(locationSelectionCoordinator?.childCoordinators.count, 0)
        
        locationSelectionCoordinator?.startRestaurantsCoordinator(with: "SW19")
        
        XCTAssertEqual(locationSelectionCoordinator?.childCoordinators.count, 1)
        XCTAssert(locationSelectionCoordinator?.childCoordinators[0] is RestaurantListCoordinator)
    }
    

}

