//
//  BaseCoordinatorTests.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import XCTest
@testable import Mangia

class BaseCoordinatorTests: XCTestCase {
    
    // MARK: Properties
    private var baseCoordinator: BaseCoordinator?
    private var mockedClient = MockedClient()
    
    override func setUp() {
        baseCoordinator = BaseCoordinator(client: mockedClient)
    }

    override func tearDown() {
        baseCoordinator = nil
    }
    
    func test_RootViewControllerNotNil_OnBaseCoordinatorInit() {
        XCTAssertNotNil(baseCoordinator?.rootViewController)
    }
    
    func test_LocationSelectionCoordinatorStartedSuccessfully_WhenBaseCoordinatorIsStarted() throws {
        
        given("The BaseCoordinator has no childCoordinators before it starts") {
            XCTAssertEqual(baseCoordinator?.childCoordinators.count, 0)
        }
        
        when("The BaseCoordinator is started") {
            baseCoordinator?.start()
        }
        
        try then("The BaseCoordinator should have one child coordinator, and it should be a LocationSelectionCoordinator") {
            XCTAssertEqual(baseCoordinator?.childCoordinators.count, 1)
            
            let firstCoordinator = try XCTUnwrap(baseCoordinator?.childCoordinators[0])
            
            XCTAssert(firstCoordinator is LocationSelectionCoordinator)
        }
    }
}

