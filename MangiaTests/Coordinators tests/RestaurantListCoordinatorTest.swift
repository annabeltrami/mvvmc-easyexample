//
//  RestaurantListCoordinatorTest.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 03/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import XCTest
@testable import Mangia

class RestaurantListCoordinatorTests: XCTestCase {
    
    // MARK: Properties
    private var restaurantListCoordinator: RestaurantListCoordinator?
    private var mockedClient = MockedClient()
    private let fakeOutcode = "SW1A 2AA"
    private let fakeDataSource = RestaurantsDataSource()
    
    override func setUp() {
        restaurantListCoordinator = RestaurantListCoordinator(client: mockedClient,
                                                              navigationController: UINavigationController(),
                                                              outcode: fakeOutcode,
                                                              dataSource: fakeDataSource)
    }

    override func tearDown() {
        restaurantListCoordinator = nil
    }

    func test_RootViewControllerNotNil_OnBaseCoordinatorInit() {
        XCTAssertNotNil(restaurantListCoordinator?.navigationController)
    }
    
    func test_LocationSelectionCoordinatorStartedSuccessfully_WhenBaseCoordinatorIsStarted() throws {
        
        given("The coordinator's navigation controller is the rootCoordinator") {
            XCTAssertEqual(restaurantListCoordinator?.navigationController.viewControllers.count, 0)
        }
        
        when("The coordinator is started") {
            restaurantListCoordinator?.start()
        }
        
        try then("The navigation controller of the coordinaror should have a viewcontroller of RestaurantListViewController type") {
            XCTAssertEqual(restaurantListCoordinator?.navigationController.viewControllers.count, 1)
            
            let topViewController = try XCTUnwrap(restaurantListCoordinator?.navigationController.viewControllers[0])
            
            guard let restaurantVC = topViewController as? RestaurantListViewController else { XCTFail("TopViewController is not RestaurantListViewController")
                return
            }
            
            let vm = try XCTUnwrap(restaurantVC.viewModel)
            XCTAssertEqual(vm.outcode, fakeOutcode)
        }
    }
}

