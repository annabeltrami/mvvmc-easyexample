//
//  LocationSelectionViewControllerTests.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 03/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

@testable import Mangia
import CoreLocation
import XCTest

class LocationSelectionViewControllerTests: XCTestCase {

    private var locationSelectionViewController: LocationSelectionViewController?
    private var mockedLocationSelectionViewModel = LocationSelectionViewModelSpy(client: MockedClient())
    
    override func setUp() {
        locationSelectionViewController = LocationSelectionViewController(viewModel: mockedLocationSelectionViewModel)
    }

    override func tearDown() {
       locationSelectionViewController = nil
    }
    
    func test_HideSubmitMethod_HidesSubviews() throws {
        locationSelectionViewController?.addressLabel.isHidden = false
        locationSelectionViewController?.addressLabel.isHidden = false
        
        locationSelectionViewController?.hideSubmit()
        
        let vc = try XCTUnwrap(locationSelectionViewController)
        XCTAssertTrue(vc.submitButton.isHidden)
        XCTAssertTrue(vc.addressLabel.isHidden)
    }
    
    func test_PostCodeInputBoxGetsEmptied_WhenInvalidPostCodeHasBeenEnteredCalled() {
        locationSelectionViewController?.postcodeInputBox.text = "Hi"
        locationSelectionViewController?.gpsButton.isSelected = true
        
        locationSelectionViewController?.invalidPostcodeEntered(errorMessage: "")
        
        XCTAssertEqual(locationSelectionViewController?.postcodeInputBox.text, "")
        XCTAssertEqual(locationSelectionViewController?.gpsButton.isSelected, false)
        
        locationSelectionViewController?.postcodeInputBox.text = "Hi"
        locationSelectionViewController?.invalidPostcodeEntered(errorMessage: "")
        
        XCTAssertNotEqual(locationSelectionViewController?.postcodeInputBox.text, "")
        XCTAssertEqual(locationSelectionViewController?.gpsButton.isSelected, false)
    }

    func test_CallingHandleSuccessfulPostcodeReceived_SetsTheCorrectAddressString() throws {
        
        let vc = try XCTUnwrap(locationSelectionViewController) //i need it unwrapped to check for isHidden without force unwrap
        vc.submitButton.isHidden = true
        vc.postcodeInputBox.text = "Hi"
        
        vc.handleSuccessfulPostcodeReceived("Downing Street")
        
        XCTAssertEqual(vc.postcodeInputBox.text, "")
        XCTAssertEqual(vc.addressLabel.text, "Deliver to Downing Street")
        XCTAssertFalse(vc.submitButton.isHidden)
        XCTAssertFalse(vc.addressLabel.isHidden)
        
    }

    func test_CallingClearTextField_ermClearsTheTextfield() throws {
        let vc = try XCTUnwrap(locationSelectionViewController)
        
        vc.postcodeInputBox.text = "Swift12"
        vc.gpsButton.isSelected = true
        vc.addressLabel.isHidden = false
        
        locationSelectionViewController?.clearTextField(vc.postcodeInputBox)
        
        XCTAssertEqual(vc.postcodeInputBox.text, "")
        XCTAssertFalse(vc.gpsButton.isSelected)
        XCTAssertTrue(vc.addressLabel.isHidden)
    }
    
    func test_SubmittingInvalidTextFieldInput_DeselectsButton() {
        locationSelectionViewController?.gpsButton.isSelected = true
        
        locationSelectionViewController?.submitTextFieldInput(text: "98A")
        
        XCTAssertEqual(locationSelectionViewController?.gpsButton.isSelected, false)
    }
    
}
