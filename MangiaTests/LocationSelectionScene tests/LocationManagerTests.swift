//
//  LocationManagerTests.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 03/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

@testable import Mangia
import CoreLocation
import XCTest

class LocationManagerTests: XCTestCase {

    private var locationManager: LocationManager?
    
    private let fakeLocationValidPostcode: CLLocation = CLLocation(latitude: 51.5033, longitude: -0.1275)
    private let fakeLocationInvalidPostcode: CLLocation = CLLocation(latitude: 55.349060, longitude: 4.804802)
    
    override func setUp() {
        locationManager = LocationManager(locationManager: MockedCLLocationManager())
    }

    override func tearDown() {
        locationManager = nil
    }

    func test_LocationManagerDelegateReturnsGeocodingError_WhenPlacemarksArrayIsEmptyAndErrorIsNotNil() {
        
        // given
        let spyDelegate = LocationManagerDelegateSpy()
        locationManager?.locationManagerDelegate = spyDelegate
        
        // when
        let dummyError = LocationManagerError.reverseGeocodingError
        locationManager?.gerPostcodeFromReverseGeocoding(placemarks: [], error: dummyError)
        
        // then
        XCTAssertEqual(spyDelegate.error, dummyError)
        XCTAssertEqual(spyDelegate.postcodeString, nil)
    }
    
    func test_LocationManagerDelegateReturnsNoPostcodeError_WhenPlacemarksHasNoPostalCode() {
     
        // given
        let spyDelegate = LocationManagerDelegateSpy()
        locationManager?.locationManagerDelegate = spyDelegate
        let expectation = XCTestExpectation()
        
        // when
        CLGeocoder().reverseGeocodeLocation(fakeLocationInvalidPostcode) { [weak self] (placemarks, error) in
            self?.locationManager?.gerPostcodeFromReverseGeocoding(placemarks: placemarks, error: nil)
            expectation.fulfill()
        }
        
        // then
        wait(for: [expectation], timeout: 5.0)
        XCTAssertEqual(spyDelegate.error, LocationManagerError.noPostcode)
    }

    func test_LocationManagerDelegateReturnsValidOutcode_WhenPlacemarksHasValidPostalCode() {
       
        // given
        let spyDelegate = LocationManagerDelegateSpy()
        locationManager?.locationManagerDelegate = spyDelegate
        let expectation = XCTestExpectation()
        
        // when
        CLGeocoder().reverseGeocodeLocation(fakeLocationValidPostcode) { [weak self] (placemarks, error) in
            self?.locationManager?.gerPostcodeFromReverseGeocoding(placemarks: placemarks, error: nil)
            expectation.fulfill()
        }
        
        //then
        wait(for: [expectation], timeout: 5.0)
        XCTAssertEqual(spyDelegate.postcodeString, "SW1A 2AA")
    }
}
