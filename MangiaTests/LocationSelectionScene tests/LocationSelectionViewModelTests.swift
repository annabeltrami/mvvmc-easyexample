//
//  LocationSelectionViewModelTests.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 27/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import XCTest
@testable import Mangia

class LocationSelectionViewModelTests: XCTestCase {
    
    // MARK: Properties
    private var locationSelectionViewModel: LocationSelectionViewModel?
    private var mockedClient: MockedClient?
    private var mockedLocationManager: MockedLocationManager?
    
    // MARK: Fakes
    
    let fakePostcode = Postcode(postcode: "SW1A 2AA", outcode: "SW1A", ced: "Downing Street")
    let fakePostcodeNoCed = Postcode(postcode: "SW1A 2AA", outcode: "SW1A", ced: nil)
    let postcodeString = "SW1A 2AA"
    let outcodeString = "SW1A"
    let addressString = "Downing Street"
    
    // MARK: Setup and teardown
    
    override func setUp() {
        mockedClient = MockedClient()
        mockedLocationManager = MockedLocationManager(locationManager: MockedCLLocationManager())
        locationSelectionViewModel = LocationSelectionViewModel(client: mockedClient!, locationService: mockedLocationManager!) // Normally i woudln't forcefully unwrap ANYTHING, however i believe in this case, it is ok
        
    }
    
    override func tearDown() {
        locationSelectionViewModel = nil
    }
    
    func test_PostcodeFormatValidationForTextField_ReturnsTrueWithValidInputsAndFalseWithInvalidInputs() throws {
        guard let vm = locationSelectionViewModel else {
            XCTFail()
            return
        }
        
        // Valid inputs
        XCTAssertTrue(vm.isPostcodeTextInputValid(text: "CB243AL"))
        XCTAssertTrue(vm.isPostcodeTextInputValid(text: "NW61DG"))
        XCTAssertTrue(vm.isPostcodeTextInputValid(text: "M11AE"))
        XCTAssertTrue(vm.isPostcodeTextInputValid(text: "EC4M7RF"))
        
        // Invalid inputs
        XCTAssertFalse(vm.isPostcodeTextInputValid(text: "NW6"))
        XCTAssertFalse(vm.isPostcodeTextInputValid(text: "@eh"))
        XCTAssertFalse(vm.isPostcodeTextInputValid(text: "11nbjhgfxr5"))
    }
    
    func test_SettingThePostcodeWithValidCED_CallsTheViewDelegateWithTheCorrectCEDString() {
        let spyDelegate = LocationSelectionViewDelegateSpy()
        locationSelectionViewModel?.viewDelegate = spyDelegate
        
        locationSelectionViewModel?.postcode = fakePostcode
        
        XCTAssertEqual(spyDelegate.addressString, addressString)
    }
    
    func test_SettingValidPostcodeWithNoCED_CallsTheViewDelegateWithTheCorrectOutcodeString() {
        let spyDelegate = LocationSelectionViewDelegateSpy()
        locationSelectionViewModel?.viewDelegate = spyDelegate
        
        locationSelectionViewModel?.postcode = fakePostcodeNoCed
        
        XCTAssertEqual(spyDelegate.addressString, outcodeString)
    }
    
    func test_NotSettingPostcode_CallsTheViewDelegateWithTheCorrectOutcodeString() {
        let spyDelegate = LocationSelectionViewDelegateSpy()
        locationSelectionViewModel?.viewDelegate = spyDelegate
        
        locationSelectionViewModel?.postcode = fakePostcodeNoCed
        
        XCTAssertEqual(spyDelegate.addressString, outcodeString)
    }
    
    func test_CallingDidReceivedErrorWithLocationError_CallsTheViewDelegateUsingTheCorrespondngRawvalueMessage() {
        let spyDelegate = LocationSelectionViewDelegateSpy()
        locationSelectionViewModel?.viewDelegate = spyDelegate
        
        locationSelectionViewModel?.didReceiveErrorFetchingUserPostcodeFromLocation(error: .noPostcode)
        
        XCTAssertEqual(spyDelegate.errorMessage, "This postcode is not valid!")
    }
    
    func test_CallingFetchAndValidateOutcodeWithNilOutcode_CallsLocationManagerFetchLocation() {
        XCTAssertEqual(mockedLocationManager?.postcodeString, nil)
        
        locationSelectionViewModel?.fetchAndValidateOutcode(postcode: nil)
        
        XCTAssertEqual(mockedLocationManager?.postcodeString, postcodeString)
    }
    
    func test_CallingFetchAndValidateOutcodeWithValidOutcode_FetchesPostcode() {
        XCTAssertEqual(mockedClient?.postcode, nil)
        
        locationSelectionViewModel?.fetchAndValidateOutcode(postcode: postcodeString)
        
        XCTAssertEqual(mockedClient?.postcode, fakePostcode)
    }
    
    func test_CallingFetchAndValidateOutcodeWithUnsuccessfulClient_ReturnsError() {
        XCTAssertEqual(mockedClient?.postcode, nil)
        XCTAssertEqual(mockedClient?.error, nil)

        mockedClient?.successful = false
        locationSelectionViewModel?.didFetchPostcodeFromUserLocation(postcodeString: postcodeString)
        
        XCTAssertEqual(mockedClient?.postcode, nil)
        XCTAssertEqual(mockedClient?.error, Client.APIError.invalidData)
    }
    
    func test_NotSettingTheOutcode_DoesNotCallTheCoordinatorDelegateOnSubmitOutcode() {
        let spyDelegate = LocationSelectionViewModelCoordinatorDelegateSpy()
        locationSelectionViewModel?.coordinatorDelegate = spyDelegate
    
        locationSelectionViewModel?.submitOutcode()
        
        XCTAssertEqual(spyDelegate.outcode, nil)
    }
    
    func test_NonNilOutcode_CallsTheCoordinatorDelegateOnSubmitOutcode() {
        let spyDelegate = LocationSelectionViewModelCoordinatorDelegateSpy()
        locationSelectionViewModel?.coordinatorDelegate = spyDelegate
    
        locationSelectionViewModel?.postcode = fakePostcode
        locationSelectionViewModel?.submitOutcode()
        
        XCTAssertEqual(spyDelegate.outcode, outcodeString)
    }
    
}

