//
//  XCTest + Convenience.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import os.log
import XCTest

// Not my code, but I know how it works and I use this to organise better my tests according to Behaviour Driven Development. Source for the code: https://medium.com/flawless-app-stories/ios-achieving-maximum-test-readability-at-no-cost-906af0dbaa98

extension XCTest {
    
    func given<A>(_ description: String, block: () throws -> A) rethrows -> A {
        os_log("1º Given %{public}@", description)
        return try XCTContext.runActivity(named: "Given " + description, block: { _ in try block() })
    }

    func when<A>(_ description: String, block: () throws -> A) rethrows -> A {
        os_log("2º When %{public}@", description)
        return try XCTContext.runActivity(named: "When " + description, block: { _ in try block() })
    }

    func then<A>(_ description: String, block: () throws -> A) rethrows -> A {
        os_log("3º Then %{public}@", description)
        return try XCTContext.runActivity(named: "Then " + description, block: { _ in try block() })
    }
}
