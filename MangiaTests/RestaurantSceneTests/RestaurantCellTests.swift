//
//  RestaurantCellTests.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 03/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import XCTest
@testable import Mangia

class RestaurantCellTests: XCTestCase {

    private var restaurantCell: RestaurantTableViewCell?
    
    let fakeCousines: [CousineType] = [CousineType(name: "Italian"), CousineType(name: "Austrian"), CousineType(name: nil)]
    
    override func setUp() {
        restaurantCell = RestaurantTableViewCell()
    }

    override func tearDown() {
        restaurantCell = nil
    }

    func test_SettingIsOpenBoolToTrue_ConfiguresViewAppropriately() {
        XCTAssertEqual(restaurantCell?.openLabel.text, nil)
        restaurantCell?.isOpen = true
        XCTAssertEqual(restaurantCell?.openLabel.text, "open".uppercased())
    }
    
    func test_SettingIsOpenBoolToFalse_ConfiguresViewAppropriately() {
        XCTAssertEqual(restaurantCell?.openLabel.text, nil)
        restaurantCell?.isOpen = false
        XCTAssertEqual(restaurantCell?.openLabel.text, "closed".uppercased())
    }

}
