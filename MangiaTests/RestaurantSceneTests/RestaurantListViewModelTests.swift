//
//  RestaurantViewModelTests.swift
//  MangiaTests
//
//  Created by Anna Beltrami on 03/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import XCTest
@testable import Mangia

class RestaurantLiatViewModelTests: XCTestCase {
    
    // MARK: Properties
    private var restaurantsViewModel: RestaurantListViewModel?
    private var mockedClient: MockedClient?
    
    let fakeRestaurants = [Restaurant(id: 1, name: "Nandos", isOpen: true, ratingStars: nil, cousines: nil, logo: nil)]
  
    // MARK: Setup and teardown
    
    override func setUp() {
       mockedClient = MockedClient()
       restaurantsViewModel = RestaurantListViewModel(client: mockedClient, outcode: "SW19", dataSource: RestaurantsDataSource())
        
    }
    
    override func tearDown() {
        restaurantsViewModel = nil
        mockedClient = nil
    }
    
    func test_FetchingRestaurantsWithValidOutcodeAndSuccessfulClient() {
        restaurantsViewModel?.fetchRestaurants()
        
        XCTAssertEqual(mockedClient?.restaurants, fakeRestaurants)
        XCTAssertEqual(mockedClient?.error, nil)
    }
    
    func test_FetchingRestaurantsWithValidOutcodeAndUnsuccessfulClient() {
        mockedClient?.successful = false
        
        restaurantsViewModel?.fetchRestaurants()
        
        XCTAssertEqual(mockedClient?.restaurants, nil)
        XCTAssertEqual(mockedClient?.error, Client.APIError.invalidData)
    }
    
}

