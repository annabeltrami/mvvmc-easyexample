//
//  RestaurantTableViewCell.swift
//  Mangia
//
//  Created by Anna Beltrami on 02/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import Cosmos
import EMTNeumorphicView
import SDWebImage
import SnapKit

// MARK: - RestaurantTableViewCell

class RestaurantTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    static let identifier = "RestaurantTableViewCell"
    
    var viewModel: RestaurantDetailViewModel? {
        didSet {
            guard let vm = viewModel else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.nameLabel.text = vm.name
                self.isOpen = vm.isOpen
                self.loadImage(urlString: vm.logoURL, imageView: self.logoImage)
                self.starsView.rating = vm.ratingStars
                self.cousinesLabel.text = vm.cousines
            }
        }
    }
    
    var isOpen: Bool? {
        didSet {
            guard let isOpen = isOpen else { return }
            isOpen ? configureOpenRestaurant(newDepth: .convex, openText: "open", color: UIColor.coolGreen) : configureOpenRestaurant(newDepth: .concave, openText: "closed", color: UIColor.softRed)
        }
    }
    
    // MARK: UIProperties
    
    lazy var containerView: EMTNeumorphicView = {
        let view = EMTNeumorphicView()
        view.neumorphicLayer?.configure(depth: .convex)
        view.neumorphicLayer?.cornerType = .all
        return view
    }()
    
    lazy var imageContainerView: EMTNeumorphicView = {
        let view = EMTNeumorphicView()
        view.neumorphicLayer?.configure(depth: .convex)
        view.neumorphicLayer?.edged = true
        view.layer.cornerRadius = Style.logoContainerCornerRadius
        return view
    }()
    
    lazy var logoImage: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = Style.logoCornerRadius
        return imageView
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .prussianBlue
        label.numberOfLines = 0
        label.textAlignment = .natural
        label.font = .buttonFont
        return label
    }()

    lazy var shineView: EMTNeumorphicView = {
        let view = EMTNeumorphicView()
        view.neumorphicLayer?.elementDepth = Style.neumorphicDepth
        view.neumorphicLayer?.backgroundColor = UIColor.white.cgColor
        view.neumorphicLayer?.cornerRadius = frame.size.width / 2
        return view
    }()
    
    lazy var starsView: CosmosView = {
        let stars = CosmosView()
        stars.settings.updateOnTouch = false
        stars.settings.starMargin = 5
        stars.settings.fillMode = .half
        stars.settings.filledImage = Style.stars(mode: .filled)
        stars.settings.emptyImage = Style.stars(mode: .empty)
        return stars
    }()
    
    lazy var cousinesLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = .detailFont
        return label
    }()
    
    lazy var openLabel: UILabel = {
        let label = UILabel()
        label.textColor = .softRed
        label.font = .detailFont
        return label
    }()
    
    // MARK: Initalisation

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Layout
    
    func layout() {
        contentView.backgroundColor = .offWhite
        contentView.addSubview(containerView)
        
        [imageContainerView, nameLabel, cousinesLabel, starsView, openLabel].forEach { containerView.addSubview($0) }
      
        imageContainerView.addSubview(logoImage)
        imageContainerView.addSubview(shineView)
        
        containerView.snp.makeConstraints({ make in
            make.edges.equalToSuperview().inset(Style.smallOffset)
        })
        
        imageContainerView.snp.makeConstraints({ make in
            make.height.width.equalTo(Style.logoContainerDimension)
            make.top.left.equalToSuperview().offset(Style.margin)
        })
        
        logoImage.snp.makeConstraints({ make in
            make.height.width.equalTo(Style.logoDimension)
            make.centerY.centerX.equalToSuperview()
        })
        
        shineView.snp.makeConstraints({ make in
            make.height.width.equalTo(Style.margin)
            make.top.left.equalToSuperview().offset(Style.smallOffset)
        })
        
        nameLabel.snp.makeConstraints({ make in
            make.left.equalTo(imageContainerView.snp.right).offset(Style.margin)
            make.centerY.equalTo(imageContainerView.snp.centerY)
            make.right.equalToSuperview().offset(-Style.margin)
        })
        
        cousinesLabel.snp.makeConstraints({ make in
            make.left.equalTo(nameLabel.snp.left)
            make.right.equalTo(nameLabel.snp.right)
            make.top.equalTo(nameLabel.snp.bottom).offset(Style.verySmallOffset)
            make.height.equalTo(Style.margin)
        })
        
        starsView.snp.makeConstraints({ make in
            make.left.equalTo(imageContainerView.snp.left)
            make.width.equalTo(containerView.snp.width).multipliedBy(0.5)
            make.bottom.equalTo(containerView.snp.bottom).offset(-Style.margin)
        })
        
        openLabel.snp.makeConstraints({ make in
            make.left.greaterThanOrEqualTo(starsView.snp.right).offset(Style.margin)
            make.right.equalTo(containerView.snp.right).offset(-Style.margin)
            make.centerY.equalTo(starsView.snp.centerY)
        })
        
    }
    
    func loadImage(urlString: String?, imageView: UIImageView) {
        guard let url = urlString else { return }
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageView.sd_setImage(with: URL(string: url))
    }
    
    func configureOpenRestaurant(newDepth: EMTNeumorphicLayerDepthType, openText: String, color: UIColor) {
        containerView.neumorphicLayer?.depthType = newDepth
        openLabel.text = openText.uppercased()
        openLabel.textColor = color
    }
}

