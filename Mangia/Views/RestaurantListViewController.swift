//
//  RestaurantListViewController.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import EMTNeumorphicView
import Toast_Swift

// MARK: - RestaurantListViewController

class RestaurantListViewController: UIViewController, UITableViewDelegate{
    
    // MARK: Properties
    
    let viewModel: RestaurantListViewModel?
    var dataSource: RestaurantsDataSource?
    
    // MARK: UIProperties
    
    lazy var tableView: UITableView = {
        let tableview = UITableView()
        tableview.separatorStyle = .none
        tableview.backgroundColor = .offWhite
        return tableview
    }()
    
    lazy var stickyHeaderView: EMTNeumorphicView = {
        let view = EMTNeumorphicView()
        view.neumorphicLayer?.elementBackgroundColor = UIColor.titleRed.cgColor
        view.neumorphicLayer?.depthType = .convex
        view.neumorphicLayer?.elementDepth = Style.neumorphicDepth
        view.neumorphicLayer?.cornerRadius = Style.corner
        view.neumorphicLayer?.edged = true
        return view
    }()

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = .titleFont
        return label
    }()

    lazy var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.style = .large
        spinner.hidesWhenStopped = true
        return spinner
    }()
    
    // MARK: Initialisation
    
    init(viewModel: RestaurantListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel?.viewDelegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        view.backgroundColor = .offWhite
       
        layout()
        spinner.startAnimating()
        titleLabel.text = Strings.restaurants + (viewModel?.outcode ?? "you")
        tableView.dataSource = self.dataSource
        tableView.delegate = self
        tableView.register(RestaurantTableViewCell.self, forCellReuseIdentifier: RestaurantTableViewCell.identifier)
        dataSource?.data.addAndNotify(observer: self) { [weak self] in
            self?.tableView.reloadData()
        }

        viewModel?.fetchRestaurants()
    }
    
    func layout() {
        [tableView, spinner].forEach { view.addSubview($0)}
        view.addSubview(stickyHeaderView)
        stickyHeaderView.addSubview(titleLabel)

        spinner.snp.makeConstraints({ make in
            make.centerX.centerY.equalToSuperview()
        })
        
        stickyHeaderView.snp.makeConstraints({ make in
            make.top.equalToSuperview().offset(Style.regularOffset)
            make.left.equalToSuperview().offset(Style.verySmallOffset)
            make.right.equalToSuperview().offset(-Style.verySmallOffset)
            make.height.equalTo(Style.largeOffset)
        })

        titleLabel.snp.makeConstraints({ make in
            make.centerX.centerY.equalToSuperview()
        })
        
        tableView.snp.makeConstraints({ make in
            make.edges.equalToSuperview()
        })
        
        tableView.contentInset = UIEdgeInsets(top: CGFloat(Style.veryLargeOffset), left: 0, bottom: 0, right: 0)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Style.cellHeight
    }
}

extension RestaurantListViewController: RestaurantListViewDelegate {
    
    func finishedLoadingRestaurants() {
        spinner.stopAnimating()
    }
    
    func failedToLoadRestaurants(errorMessage: String?) {
        DispatchQueue.main.async { [weak self] in
            self?.view.hideAllToasts()
            self?.view.makeToast(errorMessage, duration: Style.toastDuration, position: .center, style: Style.badToast)
        }
    }
}
