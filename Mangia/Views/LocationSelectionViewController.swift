//
//  LocationSelectionViewController.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import EMTNeumorphicView
import SnapKit
import Toast_Swift

// MARK: - LocationSelectionViewController

class LocationSelectionViewController: UIViewController {
    
    // MARK: Properties
    
    var viewModel: LocationSelectionViewModel?
    
    // MARK: UIProperties
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.welcomeLabel
        label.textColor = .titleRed
        label.numberOfLines = 0
        label.textAlignment = .natural
        label.font = .bigCoolFont
        return label
    }()
    
    lazy var textFieldContainerView: EMTNeumorphicView = {
        let view = EMTNeumorphicView()
        view.neumorphicLayer?.configure(depth: .concave)
        return view
    }()
    
    lazy var postcodeInputBox: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .none
        textField.placeholder = Strings.textFieldHint
        textField.textAlignment = .center
        textField.returnKeyType = .go
        textField.backgroundColor = .clear
        textField.textColor = .prussianBlue
        textField.font = .buttonFont
        return textField
    }()
    
    lazy var gpsButton: EMTNeumorphicButton = {
        let button = EMTNeumorphicButton(type: .custom)
        button.neumorphicLayer?.configure(depth: .convex)
        button.setImage(UIImage(systemName: "location"), for: .normal)
        button.setImage(UIImage(systemName: "location.fill"), for: .selected)
        button.tintColor = .prussianBlue
        button.addTarget(self, action: #selector(getLocation), for: .touchUpInside)
        return button
    }()
    
    lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.numberOfLines = 0
        label.textAlignment = .natural
        label.font = .lightFont
        return label
    }()
    
    lazy var submitButton: EMTNeumorphicButton = {
        let button = EMTNeumorphicButton()
        button.setTitle(Strings.submitButtonTitle, for: .normal)
        button.setTitleColor(.prussianBlue, for: .normal)
        button.titleLabel?.font = .buttonFont
        button.neumorphicLayer?.configure(depth: .convex)
        button.addTarget(self, action: #selector(submit), for: .touchUpInside)
        return button
    }()
    
    // MARK: Initialisation
    
    init(viewModel: LocationSelectionViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel?.viewDelegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .offWhite
        postcodeInputBox.delegate = self
        addSubviews()
        layout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Layout
    
    func addSubviews() {
        view.addSubview(titleLabel)
        view.addSubview(textFieldContainerView)
        view.addSubview(gpsButton)
        view.addSubview(addressLabel)
        view.addSubview(submitButton)
        textFieldContainerView.addSubview(postcodeInputBox)
        
        hideSubmit()
    }
    
    func layout() {
        
        titleLabel.snp.makeConstraints({ make in
            make.top.equalToSuperview().offset(Style.enormeousOffset)
            make.left.equalToSuperview().offset(Style.margin)
            make.right.equalToSuperview().offset(-Style.largeOffset)
        })
        
        textFieldContainerView.snp.makeConstraints({ make in
            make.width.equalToSuperview().multipliedBy(Style.textFieldWidthMultiplier)
            make.left.equalToSuperview().offset(Style.margin)
            make.top.equalTo(titleLabel.snp.bottom).offset(Style.regularOffset)
            make.height.equalTo(Style.regularOffset)
        })
        
        postcodeInputBox.snp.makeConstraints({ make in
            make.edges.equalToSuperview().inset(Style.verySmallOffset)
        })
        
        gpsButton.snp.makeConstraints({ make in
            make.centerY.equalTo(textFieldContainerView.snp.centerY)
            make.height.equalTo(textFieldContainerView.snp.height)
            make.left.equalTo(textFieldContainerView.snp.right).offset(Style.margin)
            make.right.equalToSuperview().offset(-Style.margin)
        })
        
        addressLabel.snp.makeConstraints({ make in
            make.top.equalTo(textFieldContainerView.snp.bottom).offset(Style.largeOffset)
            make.centerX.equalToSuperview()
        })
        
        submitButton.snp.makeConstraints({ make in
            make.centerX.equalToSuperview()
            make.top.equalTo(addressLabel.snp.bottom).offset(Style.margin)
            make.width.equalTo(Style.hugeOffset)
            make.height.equalTo(Style.regularOffset)
        })
    }
    
    // MARK: Actions
    
    @objc fileprivate func getLocation(sender: UIButton) {
        guard !gpsButton.isSelected else { return }
        hapticFeedback()
        gpsButton.isSelected = !gpsButton.isSelected
        postcodeInputBox.resignFirstResponder()
        viewModel?.fetchAndValidateOutcode(postcode: nil)
    }
    
    @objc fileprivate func submit(sender: UIButton) {
        hapticFeedback()
        viewModel?.submitOutcode()
    }
    
    func hapticFeedback() {
        let selectionFeedbackGenerator = UISelectionFeedbackGenerator()
        selectionFeedbackGenerator.selectionChanged()
    }
    
    func hideSubmit() {
        [submitButton, addressLabel].forEach { $0?.isHidden = true }
    }
    
    func invalidPostcodeEntered(errorMessage: String?) {
        view.hideAllToasts()
        view.makeToast(errorMessage ?? Strings.invalidPostcode, duration: Style.toastDuration, position: .top, style: Style.badToast)
        hideSubmit()
        if gpsButton.isSelected {
            postcodeInputBox.text = ""
            gpsButton.isSelected = false
        }
    }
    
    func handleSuccessfulPostcodeReceived(_ address: String) {
        view.hideAllToasts()
        view.makeToast("✔︎", duration: Style.shortToastDuration, position: .top, style: Style.goodToast)
        [submitButton, addressLabel].forEach { $0?.isHidden = false }
        addressLabel.text = Strings.addressLabel + address
        postcodeInputBox.text = ""
    }
    
    func clearTextField(_ textField: UITextField) {
        hideSubmit()
        gpsButton.isSelected = false
        textField.text = ""
    }
    
    func submitTextFieldInput(text: String?) {
        if let input = text?.replacingOccurrences(of: " ", with: ""), !input.isEmpty {
            viewModel?.isPostcodeTextInputValid(text: input) == true ? viewModel?.fetchAndValidateOutcode(postcode: input) : invalidPostcodeEntered(errorMessage: nil)
        }
    }
    
}

// MARK: - UITextFieldDelegate

extension LocationSelectionViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearTextField(textField)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        submitTextFieldInput(text: textField.text)
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - LocationSelectionViewDelegate

extension LocationSelectionViewController: LocationSelectionViewDelegate {
    
    func failedToFetchPostcode(errorMessage: String?) {
        DispatchQueue.main.async { [weak self] in
            self?.invalidPostcodeEntered(errorMessage: errorMessage)
        }
    }
    
    func didFetchPostcode(for address: String) {
        DispatchQueue.main.async { [weak self] in
            self?.handleSuccessfulPostcodeReceived(address)
        }
    }
}

extension EMTNeumorphicLayer {
    
    func configure(depth: EMTNeumorphicLayerDepthType) {
        elementBackgroundColor = UIColor.offWhite.cgColor
        depthType = depth
        elementDepth = Style.neumorphicDepth
        cornerRadius = Style.corner
    }
    
}



//TODO: fix error message being sent to restauranrlist!!

