//
//  Environment.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import Foundation

/// Environment struct contains helper methods to fetch environmental variables and info.plist values

public struct Environment {
    
    // MARK: Info Dictionary (plist)
    
    private static let infoDictionary: [String: Any] = {
        guard let infoDict = Bundle.main.infoDictionary else { fatalError("No plist") }
        return infoDict
    }()
    
    // MARK: Base Url
    
    static let baseURL: URL = {
        guard let baseURLstring = Environment.infoDictionary["BASE_URL_STRING"] as? String else { fatalError("No Base URL in this config") }
        guard let url = URL(string: baseURLstring) else { fatalError("Not a valid url") }
        return url
    }()
    
    // MARK: AuthorisationKey

    static let authorisationKey: String = {
        guard let authKey = Environment.infoDictionary["AUTHORISATION_KEY"] as? String else { fatalError("No Base URL in this config") }
        return authKey
    }()
    
    // MARK: LocationAPI Url
    
    static let locationURL: URL = {
        guard let locationURLString = Environment.infoDictionary["LOCATION_API_URL_STRING"] as? String else { fatalError("No location URL in this config")}
        guard let url = URL(string: locationURLString) else { fatalError("Not a valid URL")}
        return url
    }()

    // MARK: Environmental Variables
    
    static let isUnitTesting: Bool = {
        return ProcessInfo.processInfo.environment["UNITTEST"] == "true"
    }()
}
