//
//  SceneDelegate.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    private var baseCoordinator: BaseCoordinator?
    let client = Client()
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        guard let windowScene = (scene as? UIWindowScene), let window = window else { return }

        window.windowScene = windowScene
        
        let baseCoordinator = BaseCoordinator(client: client )
        window.rootViewController = baseCoordinator.rootViewController
        window.makeKeyAndVisible()
        
        self.baseCoordinator = baseCoordinator
        baseCoordinator.start()
    }
}
