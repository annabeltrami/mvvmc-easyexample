//
//  LocationService.swift
//  Mangia
//
//  Created by Anna Beltrami on 24/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import CoreLocation

// MARK: - LocationManagerDelegate

protocol LocationManagerDelegate: class {
    func didFetchPostcodeFromUserLocation(postcodeString: String)
    func didReceiveErrorFetchingUserPostcodeFromLocation(error: LocationManagerError)
}

// MARK: - LocationManagerError

enum LocationManagerError: Error {
    case unauthorised
    case noPostcode
    case reverseGeocodingError
    case locationFetchFailure
}

// MARK: - LocationManager

class LocationManager: NSObject, CLLocationManagerDelegate  {
    
    let cllocationManager: CLLocationManager?
    var locationManagerDelegate: LocationManagerDelegate?
    
    init(locationManager: CLLocationManager = CLLocationManager()) {
        self.cllocationManager = locationManager
        self.cllocationManager?.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func fetchLocation() {
        cllocationManager?.delegate = self
        checkAuthorisationStatus()
    }
    
    func checkAuthorisationStatus() {
        switch CLLocationManager.authorizationStatus() {
        case .restricted, .denied:
            locationManagerDelegate?.didReceiveErrorFetchingUserPostcodeFromLocation(error: .unauthorised)
            break
        case .notDetermined:
            cllocationManager?.requestWhenInUseAuthorization()
            return
        case .authorizedWhenInUse, .authorizedAlways:
            CLLocationManager.locationServicesEnabled() ? cllocationManager?.startUpdatingLocation() : locationManagerDelegate?.didReceiveErrorFetchingUserPostcodeFromLocation(error: .unauthorised)
        @unknown default: break
        }
    }
    
    // MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkAuthorisationStatus()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0] as CLLocation
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { [weak self] (placemarks, error) in
            self?.gerPostcodeFromReverseGeocoding(placemarks: placemarks, error: error)
        }
        
        manager.stopUpdatingLocation()
    }
    
    func gerPostcodeFromReverseGeocoding(placemarks: [CLPlacemark]?, error: Error?) {
       
        guard let pms = placemarks, pms.count > 0, error == nil, let firstPlacemark = placemarks?[0] else { locationManagerDelegate?.didReceiveErrorFetchingUserPostcodeFromLocation(error: .reverseGeocodingError)
            return
        }
        
        guard let postcodeString = firstPlacemark.postalCode else {
            locationManagerDelegate?.didReceiveErrorFetchingUserPostcodeFromLocation(error: .noPostcode)
            return
        }
        
        locationManagerDelegate?.didFetchPostcodeFromUserLocation(postcodeString: postcodeString)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManagerDelegate?.didReceiveErrorFetchingUserPostcodeFromLocation(error: .locationFetchFailure)
    }
    
}
