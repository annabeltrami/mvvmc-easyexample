//
//  BaseCoordinator.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//
import UIKit

// MARK: - Coordinator Protocol

protocol Coordinator {
    func start()
    var childCoordinators: [Coordinator] { get set }
}

// MARK: - BaseCoordinator

class BaseCoordinator: Coordinator {
    
    // MARK: Properties
    let client: Client
    let rootViewController: UINavigationController
    var childCoordinators = [Coordinator]()

    init(client: Client) {
        self.client = client
        rootViewController = UINavigationController()
    }
    
    func start() {
        startLocationSelectionCoordinator()
    }
    
    func startLocationSelectionCoordinator() {
        let locationSelectionCoordinator = LocationSelectionCoordinator(client: client, navigationController: rootViewController)
        childCoordinators.append(locationSelectionCoordinator)
        locationSelectionCoordinator.start()
    }
}
