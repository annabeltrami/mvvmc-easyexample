//
//  LocationSelectionCoordinator.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import UIKit

// MARK: - LocationSelectionCoordinator

class LocationSelectionCoordinator: Coordinator {

    // MARK: Properties
    
    let client: Client
    let navigationController: UINavigationController
    var childCoordinators = [Coordinator]()
   
    // MARK: Initialiser
    
    init(client: Client, navigationController: UINavigationController) {
        self.client = client
        self.navigationController = navigationController
    }
    
    // MARK: Start
    
    func start() {
        let viewModel = LocationSelectionViewModel(client: client)
        viewModel.coordinatorDelegate = self
        let locationSelectionViewController = LocationSelectionViewController(viewModel: viewModel)
        locationSelectionViewController.title = "Location Selection vc"
        navigationController.pushViewController(locationSelectionViewController, animated: true)
    }
    
    func startRestaurantsCoordinator(with outcode: String) {
        let restaurantsListDataSource = RestaurantsDataSource()
        let restaurantListCoordinator = RestaurantListCoordinator(client: client, navigationController: navigationController, outcode: outcode, dataSource: restaurantsListDataSource)
        childCoordinators.append(restaurantListCoordinator)
        restaurantListCoordinator.start()
    }

}

extension LocationSelectionCoordinator: LocationSelectionViewModelCoordinatorDelegate {
   
    func didGetOutcodeForRestaurants(_ outcode: String) {
        startRestaurantsCoordinator(with: outcode)
    }
    
}
