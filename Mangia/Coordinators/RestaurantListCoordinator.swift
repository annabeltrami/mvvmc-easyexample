//
//  RestaurantListCoordinator.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import UIKit

// MARK: - RestaurantListCoordinator

class RestaurantListCoordinator: Coordinator {
    
    // MARK: Properties

    let client: Client
    let navigationController: UINavigationController
    var childCoordinators = [Coordinator]()
    let outcode: String?
    let restaurantsListDataSource: RestaurantsDataSource?
    
    // MARK: Initialisation
    
    init(client: Client, navigationController: UINavigationController, outcode: String, dataSource: RestaurantsDataSource) {
        self.client = client
        self.navigationController = navigationController
        self.outcode = outcode
        self.restaurantsListDataSource = dataSource
    }
    
    // MARK: - Start
    
    func start() {
        guard let outcode = outcode else { return}
        let viewModel = RestaurantListViewModel(client: client, outcode: outcode, dataSource: restaurantsListDataSource)
        let restaurantListViewController = RestaurantListViewController(viewModel: viewModel)
        restaurantListViewController.title = "Restaurants for \(outcode)"
        restaurantListViewController.dataSource = restaurantsListDataSource
        navigationController.pushViewController(restaurantListViewController, animated: true)
    }
     
}
