//
//  RestaurantsDataSource.swift
//  Mangia
//
//  Created by Anna Beltrami on 02/03/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import EMTNeumorphicView
import UIKit

class RestaurantsDataSource : GenericDataSource<Restaurant>, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"RestaurantTableViewCell", for: indexPath) as! RestaurantTableViewCell
        
        cell.viewModel = RestaurantDetailViewModel(restaurant: data.value[indexPath.row])
        
        return cell
    }
}
