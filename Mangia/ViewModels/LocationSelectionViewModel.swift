//
//  LocationSelectionViewModel.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

// MARK: - LocationSelectionCoordinatorDelegate

protocol LocationSelectionViewModelCoordinatorDelegate: class {
    func didGetOutcodeForRestaurants(_ outcode: String)
}

// MARK: - LocationSelectionViewDelegate
protocol LocationSelectionViewDelegate: class {
    func didFetchPostcode(for address: String)
    func failedToFetchPostcode(errorMessage: String?)
}

// MARK: - LocationSelectionViewModel

class LocationSelectionViewModel {
    
    // MARK: Properties

    let locationManager: LocationManager?
    let client: Client?
    weak var coordinatorDelegate: LocationSelectionViewModelCoordinatorDelegate?
    weak var viewDelegate: LocationSelectionViewDelegate?
    
    var postcode: Postcode? {
        didSet {
            if let postcode = postcode {
                viewDelegate?.didFetchPostcode(for: postcode.ced ?? "" + postcode.outcode)
            }
        }
    }
    
    // MARK: Initialisation
    
    init(client: Client, locationService: LocationManager = LocationManager()) {
        self.locationManager = locationService
        self.client = client
        self.locationManager?.locationManagerDelegate = self
    }
    
    // MARK: Methods

    func fetchAndValidateOutcode(postcode: String?) {
        
        guard let postcode = postcode else {
            locationManager?.fetchLocation()
            return
        }
        
        client?.fetchPostcode(postcode: postcode, result: { [weak self] result in
            switch result {
            case .success(let response):
                self?.postcode = response.result
            case .failure:
                self?.viewDelegate?.failedToFetchPostcode(errorMessage: Strings.invalidPostcode)
                break
            }
        })
    }
    
    func submitOutcode() {
        if let outcode = postcode?.outcode {
          coordinatorDelegate?.didGetOutcodeForRestaurants(outcode)
        }
    }
    
    //The below method is quite simplified in veryfing the validity of a postcode input. To do it properly, I would use a RegEx expression based on the government's. Here i am just making sure it is 5-7 characters long and it doesnt contain symbols
    func isPostcodeTextInputValid(text: String) -> Bool {
        return text.isAlphanumeric && text.stringLenghtIsWithinRange
    }

}

extension LocationSelectionViewModel: LocationManagerDelegate {
    
    func didFetchPostcodeFromUserLocation(postcodeString: String) {
        fetchAndValidateOutcode(postcode: postcodeString)
    }
    
    func didReceiveErrorFetchingUserPostcodeFromLocation(error: LocationManagerError) {
        var errorMessage: String
        switch error {
        case .unauthorised:
            errorMessage = Strings.unauthorisedLocationAccess
        case .noPostcode:
            errorMessage = Strings.invalidPostcode
        case .reverseGeocodingError, .locationFetchFailure:
            errorMessage = Strings.locationFetchError
        }
        
        viewDelegate?.failedToFetchPostcode(errorMessage: errorMessage)
        postcode = nil
    }
}

extension String {
    
    var isAlphanumeric: Bool {
       return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    var stringLenghtIsWithinRange: Bool {
        return !isEmpty && (count >= 5 && count <= 7)
    }
}
