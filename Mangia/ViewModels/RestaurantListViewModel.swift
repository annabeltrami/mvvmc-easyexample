//
//  RestaurantListViewModel.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import Foundation

// MARK: - LocationSelectionViewDelegate
protocol RestaurantListViewDelegate: class {
    func finishedLoadingRestaurants()
    func failedToLoadRestaurants(errorMessage: String?)
}

// MARK: - RestaurantListViewModel

class RestaurantListViewModel {
    
    // MARK: Properties
    
    let client: Client?
    let outcode: String?
    weak var viewDelegate: RestaurantListViewDelegate?
    weak var dataSource: GenericDataSource<Restaurant>?
    
    init(client: Client?, outcode: String?, dataSource: GenericDataSource<Restaurant>?) {
        self.client = client
        self.outcode = outcode
        self.dataSource = dataSource
    }
    
    func fetchRestaurants() {
        guard let outcode = outcode else { return }
        
        client?.fetchRestaurants(outcode: outcode, result: { [weak self](result: Result<RestaurantResponse, Client.APIError>) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    print(response.restaurants)
                    self?.dataSource?.data.value = response.restaurants.compactMap{ $0 }.sorted { $0.isOpen && !$1.isOpen }
                    self?.viewDelegate?.finishedLoadingRestaurants()
                case .failure(let error):
                    self?.viewDelegate?.failedToLoadRestaurants(errorMessage: error.localizedDescription)
                    break
                }
            }
        })
    }
}
