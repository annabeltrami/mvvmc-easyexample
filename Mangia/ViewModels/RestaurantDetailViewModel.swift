//
//  RestaurantDetailViewModel.swift
//  Mangia
//
//  Created by Anna Beltrami on 31/05/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

struct RestaurantDetailViewModel {
    
    // MARK: - Properties
    
    private var restaurant: Restaurant
    
    var name: String {
        return restaurant.name ?? ""
    }
    
    var isOpen: Bool {
        return restaurant.isOpen
    }
    
    var ratingStars: Double {
        return restaurant.ratingStars ?? 0.0
    }
    
    var cousines: String {
        guard let cousines = restaurant.cousines, !cousines.isEmpty else { return Strings.cousines + "other"}
        return Strings.cousines + cousines.compactMap{ $0.name}.joined(separator: ", ")
    }
   
    var logoURL: String? {
        return restaurant.logo?[0].urlString //this can remain optional, SDWebImage handles this nicely for me
    }
   
    // MARK: - Lifecycle
    
    init(restaurant: Restaurant) {
        self.restaurant = restaurant
    }
    
}
