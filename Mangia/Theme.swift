//
//  Theme.swift
//  Mangia
//
//  Created by Anna Beltrami on 27/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import Toast_Swift

extension UIColor {
    
    static let offWhite = UIColor(red: 0.92, green: 0.90, blue: 0.94, alpha: 1.0)
    static let softRed = UIColor(red: 0.96, green: 0.56, blue: 0.56, alpha: 1.0)
    static let softGreen = UIColor(red: 0.74, green: 0.93, blue: 0.63, alpha: 1.0)
    
    static let titleRed = UIColor(red: 1.00, green: 0.44, blue: 0.39, alpha: 1.0)
    static let prussianBlue = UIColor(red: 0.11, green: 0.30, blue: 0.50, alpha: 1.0)
    static let acquaBlue = UIColor( red: 0.75, green: 0.84, blue: 0.92, alpha: 1.0)
    static let lime = UIColor(red: 0.88, green: 1.00, blue: 0.31, alpha: 1.0)
    static let sunglow = UIColor(red: 0.99, green: 0.79, blue: 0.27, alpha: 1.0)
    static let coolGreen = UIColor(red: 0.19, green: 0.85, blue: 0.29, alpha: 1.0)
}

// MARK: - Style

struct Style {
    
    static let verySmallOffset = 8.0
    static let smallOffset = 15.0
    static let margin = 25.0
    static let regularOffset = 50.0
    static let largeOffset = 75.0
    static let veryLargeOffset = 100.0
    static let hugeOffset = 120.0
    static let enormeousOffset = 180.0
    
    static let textFieldWidthMultiplier = 0.65
     
    static let corner: CGFloat = 20.0
    static let neumorphicDepth: CGFloat = 9.0
    static let shallowDepth: CGFloat = 4.5
    
    // MARK: Toast!

    static let shortToastDuration = 1.0
    static let toastDuration = 4.0
    
    static var badToast: ToastStyle = {
        var style = ToastStyle()
        style.backgroundColor = .softRed
        return style
    }()
    
    static var goodToast: ToastStyle = {
        var style = ToastStyle()
        style.messageColor = .darkGray
        style.backgroundColor = .softGreen
        return style
    }()
    
    // MARK: RestaurantsCell
    
    static let logoContainerDimension: CGFloat = 60.0
    static let logoContainerCornerRadius: CGFloat = 30.0
    static let logoDimension: CGFloat = 56.0
    static let logoCornerRadius: CGFloat = 28.0
    static let cellHeight: CGFloat = 200.0
    
    
    enum StarMode: String {
        case filled = "star.fill"
        case empty = "star"
    }
    
    static func stars(mode: StarMode) -> UIImage {
        guard let image = UIImage(named: mode.rawValue)
        else { return UIImage() }
        
        return image
    }
    
}

// MARK: Font

extension UIFont {
    
    static let bigCoolFont = UIFont(name: "Avenir-HeavyOblique", size: 26.0)
    static let detailFont = UIFont(name: "Avenir-Heavy", size: 12.0)
    static let buttonFont = UIFont(name: "Avenir-Heavy", size: 16.0)
    static let lightFont = UIFont(name: "Avenir-Medium", size: 16.0)
    static let titleFont = UIFont(name: "Avenir-Heavy", size: 20.0)
    
}
