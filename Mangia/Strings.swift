//
//  Strings.swift
//  Mangia
//
//  Created by Anna Beltrami on 25/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import Foundation

struct Strings {
    
    // MARK: LocationSelectionViewController
    
    static let welcomeLabel = "Welcome! Where would you like your food to be delivered to?"
    static let textFieldHint = "Postcode eg EC4M 7RF"
    static let submitButtonTitle = "SUBMIT"
    static let invalidPostcode = "This postcode is not valid!"
    static let addressLabel = "Deliver to "
    static let unauthorisedLocationAccess = "Please allow location access"
    static let locationFetchError = "Error fetching location. Please input it manually or try again later"
    
    // MARK: RestaurantsListViewController
    static let restaurants = "Restaurants for "
    static let cousines = "Cousines: "

}

