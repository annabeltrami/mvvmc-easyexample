//
//  Restaurant.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import Foundation

// MARK: - RestaurantResponse

struct RestaurantResponse: Codable {
    let restaurants: [Restaurant]
    
    enum CodingKeys: String, CodingKey {
        case restaurants = "Restaurants"
    }
}

// MARK: - Restaurant

struct Restaurant: Codable, Equatable {
    
    let id: Int?
    let name: String?
    let isOpen: Bool
    let ratingStars: Double?
    let cousines: [CousineType]?
    let logo: [RestaurantLogo]?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case isOpen = "IsOpenNowForDelivery"
        case ratingStars = "RatingStars"
        case cousines = "CuisineTypes"
        case logo = "Logo"
    }
    
    static func == (lhs: Restaurant, rhs: Restaurant) -> Bool {
        return lhs.id == rhs.id
    }
}

// MARK: - RestaurantLogo

struct RestaurantLogo: Codable {
    
    let urlString: String?
    
    enum CodingKeys: String, CodingKey {
        case urlString = "StandardResolutionURL"
    }
}

// MARK: - CousineType

struct CousineType: Codable {
    
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
    }
}
