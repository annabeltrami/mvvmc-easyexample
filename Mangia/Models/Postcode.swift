//
//  Location.swift
//  Mangia
//
//  Created by Anna Beltrami on 26/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import Foundation

struct Postcode: Codable, Equatable {
    
    let postcode: String
    let outcode: String
    let ced: String?
    
}

struct PostcodeResponse: Codable {
    let result: Postcode
}
