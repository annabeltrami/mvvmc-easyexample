//
//  Client.swift
//  Mangia
//
//  Created by Anna Beltrami on 23/02/2020.
//  Copyright © 2020 Anna Beltrami. All rights reserved.
//

import Foundation

class Client {
    
    // MARK: Properties
    
    private let urlSession = URLSession.shared
    
    private let authKey = Environment.authorisationKey
  
    private var headers: [String: String] {
        return ["Accept-tenant": "uk",
                "Accept-language": "en-GB",
                "Authorization": "Basic \(authKey)",
                "Host": "public.je-apis.com"]
    }
    
    // MARK: Components
    
    enum Components: String {
        case restaurants
        case postcodes
    }
    
    // MARK: Error
    
    enum APIError: Error {
        case apiError
        case invalidEndpoint
        case invalidResponse
        case invalidData
        case decodeError
        case invalidRequest
    }

    // MARK: Methods
    
    func fetchRestaurants(outcode: String, result: @escaping (Result<RestaurantResponse, APIError>) -> Void) {
        var restaurantsURL = Environment.baseURL
        restaurantsURL.appendPathComponent("/\(Components.restaurants)")
        var urlComponents = URLComponents(url: restaurantsURL, resolvingAgainstBaseURL: true)
        urlComponents?.queryItems = [URLQueryItem(name: "q", value: outcode)]
        
        guard let url = urlComponents?.url else { return }
     
        fetch(with: restaurantRequest(url: url), completion: result)
    }
    
    func fetchPostcode(postcode: String, result: @escaping (Result<PostcodeResponse, APIError>) -> Void) {
         var postcodesURL = Environment.locationURL
         postcodesURL.appendPathComponent("/postcodes/\(postcode)")
         fetch(with: URLRequest(url: postcodesURL), completion: result)
    }
    
    // This function accepts a URLRequest and it returns a completion with either a generic decoded model or a custom error
    private func fetch<T: Decodable>(with request: URLRequest, completion: @escaping (Result<T, APIError>) -> Void) {
        urlSession.dataTask(with: request) { (result) in
            switch result {
            case .success(let (response, data)):
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode, 200..<299 ~= statusCode else {
                    completion(.failure(.invalidResponse))
                    return
                }
                do {
                    let values = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(values))
                } catch {
                    completion(.failure(.decodeError))
                }
            case .failure(let error):
                completion(.failure(.apiError))
                print(error)
            }
        }.resume()
    }
}

// MARK: - Request
private extension Client {
    
    func restaurantRequest(url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        return request
    }
    
}

// MARK: - Data Task

extension URLSession {
    
    func dataTask(with request: URLRequest, result: @escaping (Result<(URLResponse, Data), Error>) -> Void) -> URLSessionDataTask {
        return dataTask(with: request) { (data, response, error) in
            if let error = error {
                result(.failure(error))
                return
            }
            guard let response = response, let data = data else {
                let error = NSError(domain: "error", code: 0, userInfo: nil)
                result(.failure(error))
                return
            }
            result(.success((response, data)))
        }
    }
}
